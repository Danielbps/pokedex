# Desafio técnico

# Resumo

Foi proposto para o desafio a produção de uma aplicação possuísse um formulário para acessar a [PokeAPI](https://pokeapi.co/) e retornasse os sprites do Pokémon, analisando a API e o design inicial proposto, foi desenvolvido um design amigável e de fácil uso buscando evitar erros de entrada pelo usuário e tratar possíveis erros de requisição ou de servidor.
Segue a UI Proposta:

[UI Pokédex](https://www.figma.com/file/kOFX4Qki4e3497Eyv9NfwC/Pokedex-Desáfio-Técnico?node-id=0%3A1)

## Instruções para a execução do projeto:

### Requisitos

- [Node.Js](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/)

### Rodando a aplicação e seus testes

Com os requisitos instalados, abra o terminal, e certifique-se de que se encontra no diretório onde se encontra a aplicação e execute os seguintes comandos:
Caso utilize o windows pode ser que você tenha que executar este comando como administrador no powershell:
```powershell
Set-ExecutionPolicy -ExecutionPolicy Unrestricted
```
```powershell
# Instalar as dependencias do projeto
yarn

# Executar o projeto
yarn run start

# Executar os testes do projeto
yarn run test

# preparar a build do projeto
yarn run build
```

Grato pela atenção,
Daniel Sales.

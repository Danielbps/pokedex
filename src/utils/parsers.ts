type PokemonType = {
    number:number,
      name:string,
      sprites: {
        backDefault:string,
        backShiny:string,
        frontDefault:string,
        frontShiny:string,
      },
      spritesFemale:{
        backDefault:string,
        backShiny:string,
        frontDefault:string,
        frontShiny:string,
      },
      types:string[]
}

export function pokemonObjResponseFormatter(pokemon:any):PokemonType {
  const formatedPokemonObject ={
    number:pokemon.id,
      name:pokemon.name,
      sprites: {
        backDefault:pokemon.sprites.back_default,
        backShiny:pokemon.sprites.back_shiny,
        frontDefault:pokemon.sprites.front_default,
        frontShiny:pokemon.sprites.front_shiny,
      },
      spritesFemale: {
        backDefault:pokemon.sprites.back_female,
        backShiny:pokemon.sprites.back_shiny_female,
        frontDefault:pokemon.sprites.front_female,
        frontShiny:pokemon.sprites.front_shiny_female,
      },
      types: pokemon.types.map((type:{slot:number,type:{name:string,url:string}}) =>{
        return type.type.name;
      })
    }
    return formatedPokemonObject
}

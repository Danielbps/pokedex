import React, { useState } from "react";
import { Button, Container, Input, Label } from "./styles";

type CallbackFunction = {
  callback: (value:number) => void;
}

export function Search({callback}:CallbackFunction) {
  const [value, setValue] = useState(1)
  const handleChange = (event:React.ChangeEvent<HTMLInputElement>) => {
    let ValueVerified = Number(event.target.value.replace(/^0+/, ''));
    ValueVerified = ValueVerified>898? 898:ValueVerified;
    setValue(ValueVerified);
  }
  const handleSubmit = (event:any) =>{
    callback(value)
    event.preventDefault();
  }
  return (
    <Container onSubmit={(event)=>{handleSubmit(event)}}>
      <div>
        <Label>Enter the Pokemon number between 1 - 898:</Label>
        <Input placeholder="pokemon id" min="1" max="898" type="number" value={String(value).replace(/^0+/, '')} onChange={(event)=>handleChange(event)}></Input>
      </div>
      <Button>Search</Button>
    </Container>
  )
}

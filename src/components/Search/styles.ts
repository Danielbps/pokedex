import styled from "styled-components";

export const Container = styled.form`
display: grid;
grid-template-columns: fit-content(40%) fit-content(40%);
padding: 0rem 0rem 2rem;
@media screen and (max-width: 700px) {
  grid-template-columns: 1fr;
  grid-template-rows: 1fr 1fr;
}
`
export const Input = styled.input`
width: 30rem;
margin: 0.5rem 1rem 0rem 0rem;
height: 2.5rem;
z-index: 12;
background: var(--white);
box-shadow: 0px 4px 16px rgba(0, 0, 0, 0.25);
border-radius: 8px;
padding: 0.5rem 1rem;
border: 1px var(--gray) solid;
@media screen and (max-width: 700px) {
  width: 100%;
}
`
export const Button = styled.button`
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
align-self: flex-end;
padding: 0.5rem 1rem;
height: 2.5rem;
border: 1px var(--gray) solid;
background: #FFC703;
box-shadow: 0px 4px 16px rgba(0, 0, 0, 0.25);
border-radius: 8px;
z-index: 10;
`
export const Label = styled.label`
z-index: 10;
`
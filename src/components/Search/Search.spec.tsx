import '@testing-library/jest-dom'
import { fireEvent, render, screen } from "@testing-library/react";
import { Search } from ".";


describe("Testing Search.tsx", () => {
  const logPokemonId = jest.fn();
  // Deve ser possível exibir o elemento h1 na página
  it("verify label text", () => {
    render(<Search callback={(pokemonId)=>logPokemonId(pokemonId)} />);
    const labelText = screen.getByText('Enter the Pokemon number between 1 - 898:');
    expect(labelText).toBeInTheDocument();
  });
  it("verify button text", () => {
    render(<Search callback={(pokemonId)=>logPokemonId(pokemonId)} />);
    const labelText = screen.getByText('Search');
    expect(labelText).toBeInTheDocument();
  });
  it("verify input placeholder", () => {
    render(<Search callback={(pokemonId)=>logPokemonId(pokemonId)} />);
    const labelText = screen.getByPlaceholderText('pokemon id');
    expect(labelText).toBeInTheDocument();
  });
  it("verify button click event", async () => {
    render(<Search callback={(pokemonId)=>logPokemonId(pokemonId)} />);
    const button = screen.getByText('Search');
    await fireEvent.click(button)
    expect(logPokemonId).toHaveBeenCalled();
  });
});
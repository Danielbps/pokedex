import { useEffect, useState } from "react";
import { Container,Name,Pokemon, PokeSprites, PokeTypesDiv, Sprite, Type, PokemonNameAndSex, SelectPokemonSex } from "./styles";

export function PokeViewer({pokemon}: any) {
  const formatPokeNumber = (number: number):String =>{
    return ("0000" + number).slice(-3)
  }
  const [numberOfSprites,setNumberOfSprites] = useState(4)
  const [pokemonSex, setPokemonSex] = useState('male')
  useEffect(()=>{
    setPokemonSex('male')
    if (pokemon) {
      let selectedSprites = pokemon.sprites;
      setNumberOfSprites(Object.values(selectedSprites).filter(obj => obj!==null).length);
    }
  },[pokemon])
  return (
    <Container>
      {pokemon?
      <Pokemon>
        <PokemonNameAndSex>
          <Name>{pokemon.name}<span> Nº{formatPokeNumber(pokemon.number)}</span></Name>
          {
            Object.values(pokemon.spritesFemale).filter(url => url!== '' && url !== null ).length>0
            &&
            <SelectPokemonSex value={pokemonSex} onChange={(event)=>{ setPokemonSex(event.target.value)}}>
              <option selected value="male">Male</option>
              <option value="female">Female</option>
            </SelectPokemonSex>
          }
        </PokemonNameAndSex>
        <PokeSprites greatSize={numberOfSprites>2} pokeSprites={`repeat(${numberOfSprites}, 1fr);`}>
          {
            createSpritesElement(Object.entries(pokemonSex === 'male'? pokemon.sprites : pokemon.spritesFemale))
          }
        </PokeSprites>
        <PokeTypesDiv>
          {createTypesElement(pokemon.types)}
        </PokeTypesDiv>
      </Pokemon>
      :<h1>
        Search your Pokemon!
      </h1>}
    </Container>
  )
}


const createTypesElement = (sprites: string[]):Array<JSX.Element> => {
  const elements = sprites.map((type):JSX.Element => {
    return(
    <Type key={type} background={`var(--${type})`}>
      {type}
    </Type>
    )
  })
  return elements
}

const createSpritesElement = (sprites: [name: string, url: string][]):Array<JSX.Element | undefined> => {
  const elements = sprites.map((sprite,index):JSX.Element | undefined => {
      return(
      sprite[1]?
      <Sprite key={index}>
        <img className="sprite-img" src={sprite[1]} alt={sprite[0]} />
      </Sprite>
      :undefined
      )
  })
  return elements
}
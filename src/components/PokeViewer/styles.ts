import styled from "styled-components";

type PokeTypes = {
  background: string
}
type SpritesSize = {
  pokeSprites: string;
  greatSize:boolean;
}

export const Container = styled.div`
padding: 2rem;
display: flex;
align-items: center;
flex-direction: column;
background: var(--blue-light);
box-shadow: 0px 1px 8px rgba(0, 0, 0, 0.25);
border-radius: 1rem;
z-index: 10;
@media screen and (max-width: 700px) {
  max-width: calc(90vw - 4rem);
}
`

export const Pokemon = styled.div`
padding: 1rem;
display: flex;
align-items: center;
flex-direction: column;
`
export const Name = styled.h1`
padding:0rem 1rem;
&:first-letter {
    text-transform: uppercase;
}
span{
  color: var(--poke-number-color);
  text-transform: capitalize;
}
`
export const PokeSprites = styled.div<SpritesSize>`
display: grid;
width:100%;
padding: 0rem 0rem 4rem;
gap: 1rem;
grid-template-columns: ${props => props.pokeSprites};
@media screen and (max-width: 1000px) {
  grid-template-columns: ${props => props.greatSize? '1fr 1fr': '1fr'};
  grid-template-rows: ${props => props.greatSize? '1fr 1fr': props.pokeSprites};
}
@media screen and (max-width: 700px) {
  grid-template-columns: 1fr;
  grid-template-rows: ${props => props.pokeSprites};
}
`
export const Sprite = styled.div`
margin: 0 auto;
border-radius:50%;
background: var(--white-smoke);
box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.25);
.sprite-img{
  position: relative;
  margin: 0 auto;
}
`

export const PokeTypesDiv = styled.div`
display: grid;
gap: 1rem;
grid-template-columns: fit-content(30%) fit-content(30%);
`

export const Type = styled.div<PokeTypes>`
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
align-self: flex-end;
padding: 0.5rem 3rem;
height: 2rem;
color: #FFF;
border: 1px var(--gray) solid;
background: ${props =>(props.background)};
box-shadow: 0px 4px 16px rgba(0, 0, 0, 0.25);
border-radius: 8px;
`
export const PokemonNameAndSex = styled.div`
display: flex;
justify-content: center;
align-items: center;
padding-bottom: 1rem;
@media screen and (max-width: 700px) {
  flex-direction:column;
}
`
export const SelectPokemonSex = styled.select`
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
padding: 0.5rem 1rem;
margin: 0.5rem 0rem;
width: 10rem;
height: 2.5rem;
background: var(--blue);
border-radius: 8px;
@media screen and (max-width: 700px) {
  width: 100%;
}
`
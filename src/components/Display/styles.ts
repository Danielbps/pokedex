import styled from "styled-components";

export const Container = styled.main`
max-width: 1120px;
margin: 0 auto;
padding: 2rem 2rem 3rem;
background: var(--white-smoke);
box-shadow: 0px 1px 16px rgba(0, 0, 0, 0.5);
border-radius: 8px;
position: relative;
@media screen and (max-width: 700px) {
  max-width: 90vw;
  margin-top: 2rem;
}
.pokeball-img{
  position: absolute;
  right: 0;
  width: 10rem;
  margin-right:3rem;
}
.mobile-logo{
@media screen and (min-width: 701px) {
  display: none;
}
@media screen and (max-width: 700px) {
  position: absolute;
  right: 1rem;
  margin-top: -6rem;;
  height:3rem;
}
}
`
export const Content = styled.div`
display: grid;
grid-template-rows: fit-content(1fr) 2fr;
`
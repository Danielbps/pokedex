import { PokeViewer } from "../PokeViewer";
import { Search } from "../Search";
import { Container, Content } from "./styles";
import Pokeball from '../../assets/pokeball.svg'
import { useEffect, useState } from "react";
import { api } from "../../services/api";
import { pokemonObjResponseFormatter } from "../../utils/parsers";
import { ToastContainer, toast } from "react-toastify";
import logoImg from '../../assets/logo.svg'
import 'react-toastify/dist/ReactToastify.css';

interface IPokemon {
  number: number;
  name: string;
  sprites: {
    backDefault: string;
    backShiny: string;
    frontDefault: string;
    frontShiny: string;
  };
  spritesFemale: {
    backDefault: string;
    backShiny: string;
    frontDefault: string;
    frontShiny: string;
  },
  types: string[];
}

export function Display() {
  const [pokemonId, setPokemonId] = useState<number>()
  const [pokemon, setPokemon] = useState<IPokemon>()
  const [firtsSearch, setFirtsSearch] = useState(true);
  const [width, setWidth] = useState(window.innerWidth);
  const updateDimensions = () => {
    setWidth(window.innerWidth);
  }
  useEffect(() => {
    window.addEventListener("resize", updateDimensions);
    return () => window.removeEventListener("resize", updateDimensions);
  }, []);
  useEffect(() => {
    if (!firtsSearch) {
      api.get(`pokemon/${pokemonId}`)
        .then(response => {
          setPokemon(pokemonObjResponseFormatter(response.data))
        })
        .catch(error => {
          if(error.response?.status === 404){
            toast.error("Ooops, We can't find the Pokémon, please check the Pokémon ID number.");
          } else {
            toast.error("Unexpected error, please verify your connection or wait a feels seconds.");
          }
        })
    } else {
      setFirtsSearch(false);
    }
  }, [pokemonId])
  useEffect(() => {
    if (pokemon) {
      toast.success(`Encontramos o ${pokemon.name}`)
    }
  }, [pokemon])
  return (
    <Container>
      <img src={logoImg} className="mobile-logo" alt="logo img" />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      {
        width >= 800 ?
          <img src={Pokeball} className="pokeball-img" alt="Poke ball" /> : null
      }
      <Content>
        <Search callback={(selectedPokemonId:number) => { setPokemonId(selectedPokemonId) }} />
        <PokeViewer pokemon={pokemon} />
      </Content>
    </Container>
  )
}

import logoImg from '../../assets/logo.svg'
import blueDotImg from '../../assets/blue-dot.svg'
import redDotImg from '../../assets/red-dot.svg'
import yellowDotImg from '../../assets/yellow-dot.svg'
import greenDotImg from '../../assets/green-dot.svg'
import { Container, Content, DotDiv, LogoDiv } from './styles'


export function Header():JSX.Element {
  return (
    <Container>
      <div className='clipped-div'>
      <Content>
        <DotDiv>
          <img src={blueDotImg} alt="blue dot" />
          <img src={redDotImg} alt="red dot" />
          <img src={yellowDotImg} alt="yellow dot" />
          <img src={greenDotImg} alt="green dot" />
        </DotDiv>
        <LogoDiv>
          <img src={logoImg} className="logo" alt="logo pokedex" />
        </LogoDiv>
      </Content>
      </div>
    </Container>
  )
}

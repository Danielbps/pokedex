import styled from "styled-components";

export const Container = styled.header`
  clip-path: polygon(0 0,100% 0,100% 65%,42.5% 65%,38.5% 85%,0 85%);
  background: linear-gradient(var(--background), rgba(0, 0, 0, 0.4));
  .clipped-div{
    background: var(--background);
    clip-path: polygon(0 0,100% 0,100% 60%,42% 60%,38% 80%,0 80%);
  }
  img {
    @media screen and (max-width: 700px) {
      max-width:calc(95%/4);
    }
  }
  `
export const Content = styled.div`
max-width: 1120px;
margin: 0 auto;
display: grid;
grid-template-columns: 1fr 1fr 1fr;
padding: 2rem 1rem 3rem;
@media screen and (max-width: 700px) {
  max-width: 90vw;
  grid-template-columns: 1fr;
  padding: 0rem 0rem 3rem;
}
`
export const DotDiv = styled.div`
display:flex;
align-items: flex-start;
`
export const LogoDiv = styled.div`
display: flex;
align-items: flex-start;
justify-content: center;
img{
  height:60px;
}
@media screen and (max-width: 700px) {
  display: none;
}
`
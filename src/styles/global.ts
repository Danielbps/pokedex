import {createGlobalStyle} from 'styled-components';

export function px2rem(size:number, context = 16) {
  return size / context + 'rem'
}

export function px2vw(size:number, context = 1440) {
  return (size / context) * 100 + 'vw'
}

export const GlobalStyle = createGlobalStyle`
  :root{
    --background: #EC3E3E;

    --white-smoke: #F5F5F5;
    --blue: #66D9FF;
    --yellow: #ffd11a;

    --blue-light:#99ffff;
    --gray: #414141;
    --text-color:#515151;
    --poke-number-color:#717171;

    --water: #60B7FF;
    --fire: #FA7D29;
    --grass: #9ACC4F;
    --ice: #4EB1E9;
    --electric: #FFED37;
    --dragon: #64B3AB;
    --fighting: #D36723;
    --poison: #BE7CCD;
    --bug: #8DC64A;
    --ground: #C4AD32;
    --ghost: #7860A1;
    --flying: #9BC5D1;
    --dark: #707070;
    --psychic: #F466B9;
    --rock: #6F5F1B;
    --steel: #708585;
    --normal: #A5AAAE;
    --fairy: #FBBAEA;
  }

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  body {
    -webkit-font-smoothing:antialiased;
  }

  html, body{
    width: 100%;
    height: 100%;
    background: var(--background);
  }

  body,input,button{
    font-family: 'Ubuntu', sans-serif;
    font-weight: 400;
    color:var(--text-color);
  }

  input,button,label{
    font-size: 1.25rem;
  }

  h1,h2,h3,h4,strong{
    font-weight: 700;
  }

  html{
    @media screen and (max-width: 1080px) {
      font-size: 93.75%;
    }
    @media screen and (max-width: 720px) {
      font-size: 87.5%;
    }
  }

  button{
    cursor: pointer;
  }

  [disabled] {
    opacity: 0.6;
    cursor: not-allowed;
  }
`